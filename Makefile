.ONESHELL:

DOCKER ?= docker
REGISTRY ?= registry.gitlab.com/ethz-hpc/pipelines

kube-linter-build:
	cd images
	$(DOCKER) build . -t kube-linter:latest -f Dockerfile.kube-linter

kube-linter-push:
	$(DOCKER) tag kube-linter:latest $(REGISTRY)/kube-linter:latest
	$(DOCKER) push $(REGISTRY)/kube-linter:latest

yamllint-build:
	cd images
	$(DOCKER) build . -t yamllint:latest -f Dockerfile.yamllint

yamllint-push:
	$(DOCKER) tag yamllint:latest $(REGISTRY)/yamllint:latest
	$(DOCKER) push $(REGISTRY)/yamllint:latest

lint:
	yamllint -s .
