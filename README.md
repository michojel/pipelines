# Pipelines

This repository contains all our pipelines.

# Adding a Pipeline

You will need to create the file `.gitlab-ci.yml` in your repository. Here is an example:

```yaml
stages:
  - test
  - sast
  - build      # build and push containers

include:
  - project: "hpc/pipelines"
  # ref: "main"
    file:
      - "scripts/chart/kube-linter.yaml"
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml

chart-kube-linter:
  extends: .kube-linter
  rules:
    - if: $CI_PIPELINE_SOURCE == "push"
      changes:
        - chart/*
      when: always
  variables:
    CHART_DIRECTORY: chart/

variables:
  GIT_SUBMODULE_STRATEGY: recursive
```
